package
{
    import flash.utils.Dictionary;
    import flash.utils.getDefinitionByName;
    import org.flixel.*;

    import Mechanisms.*;
    import Enemies.*;
    import Items.*;
    import Levels.*;

    public class PlayState extends FlxState
    {
        public var currentLevel:BaseLevel;
        public var currentLevelClass:Class = null;
        public var currentClass:Class = null;
        public var nextLevelClass:Class = null;

        public var sprites:FlxGroup;
        public var player:Player;

        public var darkness:Darkness = new Darkness();
        public var status:Status = new Status();
        public var inventory:Inventory = new Inventory();

        public var Projectiles:FlxGroup = new FlxGroup();
        public var Actions:FlxGroup = new FlxGroup();
        public var Mobile:FlxGroup = new FlxGroup();
        public var Static:FlxGroup = new FlxGroup();
        public var Activable:FlxGroup = new FlxGroup();
        public var Collectable:FlxGroup = new FlxGroup();
        public var Foe:FlxGroup = new FlxGroup();
        public var Torches:FlxGroup = new FlxGroup();
        public var triggersGroup:FlxGroup = new FlxGroup;

        public var ids:Dictionary = new Dictionary(true);
        public var camera:FlxCamera;

        override public function create():void
        {
            // create projectiles to recycle
            var p:Projectile;
            for(var i:uint = 0; i < 100; i++)
            {
                p = new Projectile(-1000,-1000);
                p.exists = false;
                Projectiles.add(p);
            }
            add(Projectiles);
            for(var e:uint = 0; e < 100; e++)
            {
                p = new Projectile(-1000,-1000);
                p.exists = false;
                Actions.add(p);
            }
            add(Actions);
            //load level
            currentLevel = new currentLevelClass(true, onObjectAddedCallback);
            sprites = (currentLevel as currentLevelClass).SpritesGroup;
            FlxG.playMusic(Assets.MusicAmbiance);
            FlxG.bgColor = currentLevel.bgColor;
            //camera
            camera = new FlxCamera(0, 0, FlxG.width, FlxG.height);
            FlxG.resetCameras(camera);
            camera.follow(player);
            FlxG.worldBounds = new FlxRect(BaseLevel.boundsMinX+1, BaseLevel.boundsMinY+1, BaseLevel.boundsMaxX-BaseLevel.boundsMinX, BaseLevel.boundsMaxY-BaseLevel.boundsMinY);
            // create details
            add(darkness);
            add(status);
            add(inventory);
            //path
            setupPaths(currentLevel);
        }

        protected function onObjectAddedCallback(obj:Object, layer:FlxGroup, level:BaseLevel, scrollX:Number, scrollY:Number, properties:Array):Object
        {
            if (properties)
            {
                var i:uint = properties.length;
                while(i--)
                {
                    if (properties[i].name == "id")
                    {
                        var name:String = properties[i].value;
                        ids[name] = obj;
                        break;
                    }
                }
            }
            if (obj is TextData)
            {
                var tData:TextData = obj as TextData;
                if (tData.fontName != "" && tData.fontName != "system")
                    tData.fontName += "Font";
                return level.addTextToLayer(tData, layer, scrollX, scrollY, true, properties, onObjectAddedCallback);
            }
            else if (obj is BoxData)
            {
                // Create the trigger.
                var bData:BoxData = obj as BoxData;
                var box:Trigger = new Trigger(bData.x, bData.y, bData.width, bData.height);
                level.addSpriteToLayer(box, FlxSprite, layer, box.x, box.y, bData.angle, scrollX, scrollY);
                triggersGroup.add(box);
                if ( properties )
                {
                    i = properties.length;
                    while(i--)
                    {
                        if (properties[i].name == "target")
                        {
                            box.target = properties[i].value;
                            break;
                        }
                    }
                }
                return box;
            }
            else if (obj is ObjectLink)
            {
                var link:ObjectLink = obj as ObjectLink;
                var fromBox:Trigger = link.fromObject as Trigger;
                if (fromBox)
                {
                    fromBox.targetObject = link.toObject;
                }
            }
            else if (obj is Character)
            {
                if (obj is Player)
                    player = obj as Player;
                else if (obj is Enemy)
                    Foe.add(obj as Enemy);
                Mobile.add(obj as Character);
                //property
                i = properties.length;
                while(i--)
                {
                    if (properties[i].name == "showHealth")
                        obj.showHealth = properties[i].value;
                    else if (properties[i].name == "health")
                        obj.health = properties[i].value;
                    else if (properties[i].name == "range")
                        obj.Range = properties[i].value;
                    else if (properties[i].name == "think")
                        obj.Thinking = properties[i].value;
                }
            }
            else if (obj is Item)
            {
                Collectable.add(obj as Item);
                //property
                i = properties.length;
                while(i--)
                {
                    if (properties[i].name == "name")
                        obj.name = properties[i].value;
                }
            }
            else if (obj is Mechanism)
            {
                if (obj is Door || obj is Gate)
                    Static.add(obj as Mechanism);
                if (obj is Torch)
                    Torches.add(obj as Torch);
                Activable.add(obj as Mechanism);
                //property
                i = properties.length;
                while(i--)
                {
                    if (properties[i].name == "key")
                        obj.Key = properties[i].value;
                    if (properties[i].name == "item")
                        obj.StoredItem = properties[i].value;
                    if (properties[i].name == "active")
                        obj.Active = properties[i].value;
                    if (properties[i].name == "activator")
                        obj.Activator = properties[i].value;
                    if (properties[i].name == "expected")
                        obj.Expected = properties[i].value;
                }
            }
            return obj;
        }

        private function setupPaths(level:BaseLevel):void
        {
            for ( var i:uint = 0; i < level.paths.length; i++ )
            {
            }
        }

        override public function draw():void
        {
            darkness.fill(0xf7000000);
            super.draw();
        }

        override public function update():void
        {
            // update everything
            super.update();
            // reorder groups
            sprites.sort("y",ASCENDING);
            // map collisions
            FlxG.collide(currentLevel.hitTilemaps, Mobile);
            FlxG.collide(Mobile,Mobile);
            FlxG.collide(Mobile,Static);
            // interactions
            FlxG.overlap(Actions,Activable,Interact);
            FlxG.overlap(Projectiles,Mobile,Attack);
            FlxG.overlap(Mobile,Collectable,Collect);
            FlxG.overlap(triggersGroup, player, TriggerEntered);
            // end game
            if (!player.alive)
                FlxG.fade(0xff000000, 2, End);
        }

        public function Interact(action:Projectile, activable:Mechanism):void
        {
            activable.Interact(action.source);
        }

        public function Attack(projectile:Projectile, attacked:Character):void
        {
            if (attacked.alive && projectile.source != attacked)
                attacked.hurt(1);
            // push
            if (projectile.x < attacked.x)
                attacked.x += 2;
            else
                attacked.x -= 2;
        }

        public function Collect(character:Character, collectable:Item):void
        {
            sprites.remove(collectable, true);
            character.Take(collectable);
        }

        public function End():void
        {
            FlxG.switchState(new currentClass());
        }

        public function Continue():void
        {
            FlxG.switchState(new nextLevelClass());
        }

        // For the ClassReference trick to work the static is needed even if it's never assigned anything.
        private static var levelTutorial1:LevelTutorial1;
        private static var levelTutorial2:LevelTutorial2;
        private static var levelTutorial3:LevelTutorial3;
        private static var level1:Level1;
        private static var level2:Level2;
        private static var level3:Level3;
        private static var levelWin1:Win1;
        private static var levelWin2:Win2;
        private static var levelLoose:Loose;

        private function TriggerEntered(trigger:Trigger, plr:FlxSprite):void
        {
            var target:Object = trigger.targetObject ? trigger.targetObject : ids[trigger.target];
            try
            {
                var ClassReference:Class = getDefinitionByName("Levels." + trigger.target ) as Class;
                if (ClassReference)
                {
                    nextLevelClass = ClassReference;
                    FlxG.fade(0xff000000, 1, Continue);
                }
            }
            catch (error:Error)
            {
            }
        }

    }

}


import org.flixel.*;

class Darkness extends FlxSprite
{
    public function Darkness():void
    {
        super(0,0);
        makeGraphic(FlxG.width, FlxG.height, 0xf7000000);
        scrollFactor.x = scrollFactor.y = 0;
        blend = "multiply";
    }
}

class Status extends FlxText
{
    public function Status():void
    {
        super(FlxG.width-160-2,2,160);
        shadow = 0xff000000;
        alignment = "right";
        scrollFactor = new FlxPoint(0,0);
    }
}

class Inventory extends FlxText
{
    public function Inventory():void
    {
        super(0, FlxG.height-30, 160);
        shadow = 0xff000000;
        alignment = "left";
        scrollFactor = new FlxPoint(0,0);
    }

    override public function update():void
    {
        var player:Player = (FlxG.state as PlayState).player;
        if (player.Inventory.members.length > 0)
            text = "Inventory:\n - " + player.Inventory.members[0].name;
        else
            text = "";
        y = FlxG.height - height;
    }
}
