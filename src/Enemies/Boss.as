package Enemies
{
    import org.flixel.*;

    public class Boss extends Character
    {
        public var Thinking:Boolean = false;
        public var Active:Boolean = false;
        public var enraged:Boolean = false;

        public var rageDelay:Number = 5;
        public var rageTimer:Number = 0;

        public var enragedDelay:Number = 2;
        public var enragedTimer:Number = 0;



        public function Boss(X:Number, Y:Number):void
        {
            super(X, Y);
            loadGraphic(Assets.ImageWarrior, true, true, 32, 32, true);
            // modificar hitbox
            width = 14;
            height = 14;
            offset.x = 9;
            offset.y = 18;
            // game info
            health = 10;
            waking = true;
            showHealth = true;
            atackDelay = 1.1;
        }

        override public function update():void
        {
            super.update();
            Think();
            // for activating mechanisms
            Active = !alive;
            // for comodity
            solid = alive;
            // custom
            enragedTimer += FlxG.elapsed;
            rageTimer += FlxG.elapsed;
            if (rageTimer > rageDelay)
            {
                rageTimer = 0;
                Enrage();
            }
            if (hurted)
                Enrage();
            if ((FlxG.state as PlayState).player.hurted)
                Enrage();
        }

        public function Enrage():void
        {
            if (enragedTimer > enragedDelay)
            {
                enraged = !enraged;
                enragedTimer = 0;
            }
        }

        public function Think():void
        {
            var player:Player = (FlxG.state as PlayState).player;
            if (Thinking)
            {
                // if in range, attack
                if (waking || attacking || hurted)
                {
                    velocity.x = 0;
                    velocity.y = 0;
                }
                else
                {
                    if (enraged)
                    {
                        // pursue player
                        if (player.x > x) acceleration.x = maxVelocity.x*4;
                        if (player.x < x) acceleration.x = -maxVelocity.x*4;
                        if (player.y > y) acceleration.y = maxVelocity.y*4;
                        if (player.y < y) acceleration.y = -maxVelocity.y*4;
                        // create test projectile
                        if (player.alive)
                        {
                            var projectile:Projectile = new Projectile(0,0);
                            if (facing == RIGHT)
                                projectile.reset(x+width,y);
                            else
                                projectile.reset(x-projectile.width,y);
                            if (FlxG.overlap(projectile,player)) Attack();
                        }
                    }
                    else
                    {
                        // escape player
                        if (player.x > x) acceleration.x = -maxVelocity.x*4;
                        if (player.x < x) acceleration.x = maxVelocity.x*4;
                        if (player.y > y) acceleration.y = maxVelocity.y*4;
                        if (player.y < y) acceleration.y = -maxVelocity.y*4;
                    }
                }
            }
        }
    }
}
