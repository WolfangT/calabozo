package Enemies
{
    import org.flixel.*;

    public class Rat extends Enemy
    {
        public function Rat(X:Number, Y:Number):void
        {
            super(X, Y);
            loadGraphic(Assets.ImageRat, true, true, 32, 32, true);
            facing = LEFT;
            // modificar hitbox
            width = 14;
            height = 14;
            offset.x = 9;
            offset.y = 9;
            // modificar fisica
            maxVelocity.x = 50;
            maxVelocity.y = 50;
            health = 1;
            Range = 160;
            // modificar sonido
            SoundAttack = Assets.SoundAttackSkeleton;
        }
        override public function update():void
        {
            super.update();
            // change facing
            if (velocity.x > 0)
                facing = LEFT;
            if (velocity.x < 0)
                facing = RIGHT;
        }
    }
}
