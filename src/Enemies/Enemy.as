package Enemies
{
    import org.flixel.*;

    public class Enemy extends Character
    {
        public var Range:Number = 80;
        public var Thinking:Boolean = true;
        public var Active:Boolean = false;

        public function Enemy(X:Number, Y:Number):void
        {
            super(X, Y);
            atackDelay = 1.5;
        }

        override public function update():void
        {
            super.update();
            Think();
            // for activating mechanisms
            Active = !alive;
            // for comodity
            solid = alive;
        }

        public function Think():void
        {
            var player:Player = (FlxG.state as PlayState).player;
            var distance:Number = Math.sqrt((x-player.x)*(x-player.x) + (y-player.y)*(y-player.y));
            if (distance < Range && Thinking)
            {
                // if in range, attack
                if (waking || attacking || hurted)
                {
                    velocity.x = 0;
                    velocity.y = 0;
                }
                else
                {
                    // Pursue player
                    if (player.x > x) acceleration.x = maxVelocity.x*4;
                    if (player.x < x) acceleration.x = -maxVelocity.x*4;
                    if (player.y > y) acceleration.y = maxVelocity.y*4;
                    if (player.y < y) acceleration.y = -maxVelocity.y*4;
                    // create test projectile
                    if (player.alive)
                    {
                        var projectile:Projectile = new Projectile(0,0);
                        if (facing == RIGHT)
                            projectile.reset(x+width,y);
                        else
                            projectile.reset(x-projectile.width,y);
                        if (FlxG.overlap(projectile,player)) Attack();
                    }
                }
            }
        }
    }
}
