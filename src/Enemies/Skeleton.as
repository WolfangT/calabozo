package Enemies
{
    import org.flixel.*;

    public class Skeleton extends Enemy
    {
        public function Skeleton(X:Number, Y:Number):void
        {
            super(X, Y);
            loadGraphic(Assets.ImageSkeleton, true, true, 32, 32, true);
            // modificar hitbox
            width = 14;
            height = 14;
            offset.x = 9;
            offset.y = 18;
            // modificar fisica
            maxVelocity.x = 25;
            maxVelocity.y = 25;
            // modificar sonido
            SoundAttack = Assets.SoundAttackSkeleton;
        }
    }
}
