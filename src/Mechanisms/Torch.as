package Mechanisms
{
    import org.flixel.*;

    public class Torch extends Mechanism
    {
        public var light:Light;

        public function Torch(X:Number, Y:Number):void
        {
            super(X,Y);
            // custom
            loadGraphic(Assets.ImageTorch,true,true,16,16,true);
            addAnimation("on", [1,2,3], 24, true);
            InteracDelay = 0.2;
            Active = true;
            //light
            light = new Light(this, "p");
        }

        override public function update():void
        {
            // normal update
            super.update();
            //animation
            if (Active == Expected)
                play("on");
            else
                frame = 0;
            light.update()
        }

        override public function draw():void
        {
            super.draw();
            if (Active == Expected)
                light.draw();
        }
    }
}
