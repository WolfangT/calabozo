package Mechanisms
{
    public class DoorH extends Door
    {
        public function DoorH(X:Number,Y:Number):void
        {
            super(X,Y);
            loadGraphic(Assets.ImageDoorH,true,true,16,64,true);
            width = 4;
            offset.x = 12;
            height = 32;
            offset.y = 32;
            SoundDeactivate = Assets.SoundDeactivate;
            SoundActivate= Assets.SoundActivate;
        }
    }
}
