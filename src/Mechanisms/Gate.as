package Mechanisms
{
    import Assets;

    public class Gate extends Door
    {
        public function Gate(X:Number,Y:Number):void
        {
            super(X,Y);
            loadGraphic(Assets.ImageGate,true,true,32,48,true);
            height = 4;
            offset.y = 44;
            SoundDeactivate = Assets.SoundDeactivate;
            SoundActivate= Assets.SoundActivate;
        }

        override public function Interact(character:Character=null):void
        {
            if (!character)
                super.Interact();
        }
    }
}
