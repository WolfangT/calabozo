package Mechanisms
{
    import org.flixel.*;

    public class Mechanism extends FlxSprite
    {
        public var SoundActivate:Class = Assets.SoundActivate;
        public var SoundDeactivate:Class = Assets.SoundDeactivate;

        public var Active:Boolean = false;
        public var InteracDelay:Number = 1;
        public var LastActivated:Number = 0;

        public var Expected:Boolean = true;
        public var Activator:String;

        public function Mechanism(X:Number,Y:Number):void
        {
            super(X,Y);
            immovable = true;
        }

        override public function update():void
        {
            LastActivated += FlxG.elapsed;
            if (Active == Expected)
                frame = 1;
            else
                frame = 0;
            // activators
            if (Activator)
            {
                var activator = (FlxG.state as PlayState).ids[Activator];
                if (activator.Active != Active)
                    Interact();
            }
        }

        public function Interact(character:Character=null):void
        {
            if (LastActivated > InteracDelay)
            {
                if (Active)
                {
                    Active = false;
                    FlxG.play(SoundActivate);
                } else
                {
                    Active = true;
                    FlxG.play(SoundDeactivate);
                }
                LastActivated = 0;
            }
        }
    }
}
