package Mechanisms
{
    public class DoorF extends Door
    {
        public function DoorF(X:Number,Y:Number):void
        {
            super(X,Y);
            loadGraphic(Assets.ImageDoorF,true,true,32,48,true);
            height = 4;
            offset.y = 32;
            SoundDeactivate = Assets.SoundDeactivate;
            SoundActivate= Assets.SoundActivate;
        }
    }
}
