package Mechanisms
{
    import org.flixel.*;
    import Items.*;

    public class Door extends Mechanism
    {
        public var Key:String;

        public function Door(X:Number,Y:Number):void
        {
            super(X,Y);
            loadGraphic(Assets.ImageDoor, true, true, 32, 48, true);
            height = 32;
            SoundActivate = Assets.SoundActivateDoor;
            SoundDeactivate = Assets.SoundDeactivateDoor;
        }

        override public function update():void
        {
            super.update()
            if (Active) solid = false;
            else solid = true;
        }

        override public function Interact(character:Character=null):void
        {
            if (Key && character)
            {
                var key:Item = (FlxG.state as PlayState).ids[Key];
                var inventory:FlxGroup = character.Inventory;
                if (key == inventory.members[0])
                {
                    inventory.members.splice(0, 1);
                    super.Interact(character);
                }
            }
            else
                super.Interact(character);
        }
    }
}
