package Mechanisms
{
    import org.flixel.*;

    public class AutoTorch extends Torch
    {
        public function AutoTorch(X:Number, Y:Number):void
        {
            super(X,Y);
            Active = false;
        }

        override public function update():void
        {
            //normal update
            super.update();
            // automatic turn
            var player:Player = (FlxG.state as PlayState).player;
            if (Math.sqrt((x-player.x)*(x-player.x) + (y-player.y)*(y-player.y)) < 48)
                Active = true;
        }
    }
}
