package Mechanisms
{
    public class Lever extends Mechanism
    {
        public function Lever(X:Number, Y:Number):void
        {
            super(X,Y);
            loadGraphic(Assets.ImageLever,true,true,16,16,true);
        }
    }
}
