package Mechanisms
{
    public class DoorHF extends Door
    {
        public function DoorHF(X:Number,Y:Number):void
        {
            super(X,Y);
            loadGraphic(Assets.ImageDoorHF,true,true,16,64,true);
            width = 4;
            offset.x = 0;
            height = 32;
            offset.y = 32;
            SoundDeactivate = Assets.SoundDeactivate;
            SoundActivate= Assets.SoundActivate;
        }
    }
}
