package Mechanisms
{
    import Items.*;
    import org.flixel.*;

    public class Chest extends Mechanism
    {
        public var pos:Array = [AtackSpeedPotion, DeathPotion, LifePotion, ResistencePotion, SpeedPotion];

        public var StoredItem:String = null;

        public var opened:Boolean = false;

        public function Chest(X:Number, Y:Number):void
        {
            super(X, Y);
            loadGraphic(Assets.ImageChest, true, true, 16, 16, true);
            SoundActivate = Assets.SoundActivateDoor;
            SoundDeactivate = Assets.SoundDeactivateDoor;
        }

        override public function update():void
        {
            super.update()
            if (Active && !opened)
            {
                var player:Player = (FlxG.state as PlayState).player;
                var item:Item = generate();
                player.Take(item);
                opened = true;
            }
        }

        public function generate():Item
        {
            var item:Item;
            if (StoredItem)
                item = (FlxG.state as PlayState).ids[StoredItem];
            else
                item = new pos[int(Math.random() * (pos.length+1))](x,y);
            return item;
        }


    }
}
