package
{
    import org.flixel.*;

    public class Projectile extends FlxSprite
    {
        public var LifeTime:Number = 0;
        public var source:Character;

        public function Projectile(X:Number, Y:Number):void
        {
            super(X,Y);
            makeGraphic(16, 16, 0xffffffff);
            visible = false;
        }

        override public function update():void
        {
            // kill as soon as posible
            if (LifeTime > 0)
            {
                kill();
                source = null;
            }
            LifeTime += FlxG.elapsed;
            // update normaly
            super.update()
        }
    }
}
