package Levels
{
    public class LevelTutorial3 extends PlayState
    {
        public function LevelTutorial3():void
        {
            currentLevelClass = Level_Tutorial3;
            currentClass = LevelTutorial3;
        }

        override public function update():void
        {
            //normal update
            super.update();
        }
    }
}
