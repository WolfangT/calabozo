//Code generated with DAME. http://www.dambots.com

package Levels
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	// Custom imports:
import Mechanisms.*;import Enemies.*;import Items.*;
	public class Level_Tutorial2 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../../assets/levels/mapCSV_Tutorial2_Floor.csv", mimeType="application/octet-stream")] public var CSV_Floor:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Floor:Class;
		[Embed(source="../../assets/levels/mapCSV_Tutorial2_Walls.csv", mimeType="application/octet-stream")] public var CSV_Walls:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Walls:Class;
		[Embed(source="../../assets/levels/mapCSV_Tutorial2_Objects.csv", mimeType="application/octet-stream")] public var CSV_Objects:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Objects:Class;
		[Embed(source="../../assets/levels/mapCSV_Tutorial2_Roof.csv", mimeType="application/octet-stream")] public var CSV_Roof:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Roof:Class;

		//Tilemaps
		public var layerFloor:FlxTilemap;
		public var layerWalls:FlxTilemap;
		public var layerObjects:FlxTilemap;
		public var layerRoof:FlxTilemap;

		//Sprites
		public var SpritesGroup:FlxGroup = new FlxGroup;

		//Shapes
		public var TrigersGroup:FlxGroup = new FlxGroup;

		//Properties


		public function Level_Tutorial2(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerFloor = addTilemap( CSV_Floor, Img_Floor, 0.000, 0.000, 16, 16, 1.000, 1.000, true, 282, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerWalls = addTilemap( CSV_Walls, Img_Walls, 0.000, 0.000, 16, 16, 1.000, 1.000, false, 1, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerObjects = addTilemap( CSV_Objects, Img_Objects, 0.000, 0.000, 16, 16, 1.000, 1.000, true, 1, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerRoof = addTilemap( CSV_Roof, Img_Roof, 0.000, 0.000, 16, 16, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(layerFloor);
			masterLayer.add(layerWalls);
			masterLayer.add(layerObjects);
			masterLayer.add(TrigersGroup);
			masterLayer.add(SpritesGroup);
			masterLayer.add(layerRoof);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 640;
			boundsMaxY = 480;
			boundsMin = new FlxPoint(0, 0);
			boundsMax = new FlxPoint(640, 480);
			bgColor = 0xff000000;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addShapesForLayerTrigers(onAddCallback);
			addSpritesForLayerSprites(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addShapesForLayerTrigers(onAddCallback:Function = null):void
		{
			var obj:Object;

			callbackNewData(new TextData(62.000, 112.000, 68.000, 16.000, 0.000, "Nightmares!","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(32.000, 384.000, 32.000, 48.000, 0.000, "They must not scape","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(224.000, 279.000, 31.370, 62.750, 0.000, "They want to kill me","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(225.000, 165.000, 32.000, 48.000, 0.000, "It is all their fault!","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(320.000, 112.000, 112.000, 16.000, 0.000, "they made me do it!","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			obj = new BoxData(592.000, 224.000, 0.000, 32.000, 32.000, TrigersGroup );
			shapes.push(obj);
			callbackNewData( obj, onAddCallback, TrigersGroup, generateProperties( { name:"target", value:"LevelTutorial3" }, null ), 1, 1 );
			callbackNewData(new TextData(416.000, 320.000, 32.000, 48.000, 0.000, "This holds my hope\r","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
		}

		public function addSpritesForLayerSprites(onAddCallback:Function = null):void
		{
			addSpriteToLayer(null, LifePotion, SpritesGroup , 576.000, -80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"potion1" }, null ), onAddCallback );//"lifePotion"
			addSpriteToLayer(null, Key, SpritesGroup , 592.000, -80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"key1" }, { name:"name", value:"Prison Key" }, null ), onAddCallback );//"key"
			addSpriteToLayer(null, Torch, SpritesGroup , 368.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 416.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 320.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 272.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 176.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 128.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 80.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 32.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 416.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 368.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 320.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 272.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 176.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 128.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 80.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 32.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 80.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 368.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 320.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 272.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 224.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 176.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 128.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 80.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 32.000, 61.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 64.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 112.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 160.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 304.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 352.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 368.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 320.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 272.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 224.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 176.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 128.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 80.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 416.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Gate, SpritesGroup , 32.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 80.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 128.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 176.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 224.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 272.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 320.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 368.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 416.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 72.000, 120.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 105.000, 122.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 356.000, 124.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 384.000, 125.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 329.000, 125.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 404.000, 126.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Lever, SpritesGroup , 424.000, 168.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"lever1" }, { name:"activator", value:"torch1" }, null ), onAddCallback );//"lever"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 222.000, 172.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 244.000, 174.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 16.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 352.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 304.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 160.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 112.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 272.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 368.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 176.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 80.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 417.000, 199.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 224.000, 205.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 240.000, 206.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 608.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 576.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 544.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 512.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 480.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"torch1" }, { name:"activator", value:"lever1" }, { name:"active", value:false }, { name:"expected", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 448.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Gate, SpritesGroup , 368.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 272.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 176.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 80.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Player, SpritesGroup , 16.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"health", value:2 }, null ), onAddCallback );//"player"
			addSpriteToLayer(null, Door, SpritesGroup , 320.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"key", value:"key1" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 425.000, 233.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 383.000, 239.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, GateHF, SpritesGroup , 448.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 40.000, 258.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 243.000, 267.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 223.000, 268.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Gate, SpritesGroup , 80.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 368.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 272.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 176.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 414.000, 285.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 434.000, 285.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 80.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 368.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 272.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 176.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Chest, SpritesGroup , 40.000, 296.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"item", value:"potion1" }, null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 424.000, 296.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"item", value:"key1" }, null ), onAddCallback );//"chest"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 221.000, 296.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 242.000, 298.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 27.000, 301.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 52.000, 303.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 352.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 304.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 160.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 112.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 64.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 414.000, 307.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 433.000, 308.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 244.000, 322.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 222.000, 323.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 413.000, 335.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 435.000, 335.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 42.000, 342.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 302.000, 349.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 206.000, 350.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 108.000, 351.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 160.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 255.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 353.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 355.000, 353.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 414.000, 355.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 433.000, 356.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 33.000, 370.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 50.000, 371.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Gate, SpritesGroup , 416.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 368.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 320.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 272.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 224.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 176.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 128.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Gate, SpritesGroup , 80.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 51.000, 396.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 31.000, 397.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 432.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 384.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 336.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 288.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 240.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 192.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 144.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 96.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 416.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 368.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 320.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 272.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 224.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 176.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 128.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 80.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:false }, { name:"health", value:1 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 34.000, 419.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 48.000, 421.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 432.000, 432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 384.000, 432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 336.000, 432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 288.000, 432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 240.000, 432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 192.000, 432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 144.000, 432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 96.000, 432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
