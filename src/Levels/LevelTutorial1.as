package Levels
{
    import PlayState;

    public class LevelTutorial1 extends PlayState
    {
        public function LevelTutorial1():void
        {
            currentLevelClass = Level_Tutorial1;
            currentClass = LevelTutorial1;
        }

        override public function update():void
        {
            //normal update
            super.update();
            //custom level stuff
        }
    }
}
