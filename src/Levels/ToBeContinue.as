package Levels
{
    import org.flixel.*;

    public class ToBeContinue extends FlxState
    {
        public var timer:Number = 0;
        public var t1:FlxText;
        public var t2:FlxText;
        public var t3:FlxText;

        override public function create():void
        {
            FlxG.bgColor = 0xff000000;
            FlxG.playMusic(Assets.MusicAmbiance);

            t1 = new FlxText(FlxG.width/2-50,10,100,"To");
            t1.alignment = "center";

            t2 = new FlxText(FlxG.width/2-50,40,100,"Be");
            t2.alignment = "center";

            t3 = new FlxText(FlxG.width/2-100,70,200,"Continue");
            t3.size = 25;
            t3.alignment = "center";
        }

        override public function update():void
        {
            timer += FlxG.elapsed;
            if (timer > 1) add(t1);
            if (timer > 2) add(t2);
            if (timer > 3) add(t3);
            if (timer > 5 || FlxG.keys.ENTER)
            {
                FlxG.switchState(new Credits());
                FlxG.music.stop();
            }
            super.update();
        }
    }
}
