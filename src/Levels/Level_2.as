//Code generated with DAME. http://www.dambots.com

package Levels
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	// Custom imports:
import Mechanisms.*;import Enemies.*;import Items.*;
	public class Level_2 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../../assets/levels/mapCSV_2_Floor.csv", mimeType="application/octet-stream")] public var CSV_Floor:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Floor:Class;
		[Embed(source="../../assets/levels/mapCSV_2_Walls.csv", mimeType="application/octet-stream")] public var CSV_Walls:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Walls:Class;
		[Embed(source="../../assets/levels/mapCSV_2_Objects.csv", mimeType="application/octet-stream")] public var CSV_Objects:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Objects:Class;
		[Embed(source="../../assets/levels/mapCSV_2_Roof.csv", mimeType="application/octet-stream")] public var CSV_Roof:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Roof:Class;

		//Tilemaps
		public var layerFloor:FlxTilemap;
		public var layerWalls:FlxTilemap;
		public var layerObjects:FlxTilemap;
		public var layerRoof:FlxTilemap;

		//Sprites
		public var SpritesGroup:FlxGroup = new FlxGroup;

		//Shapes
		public var TrigersGroup:FlxGroup = new FlxGroup;

		//Properties


		public function Level_2(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerFloor = addTilemap( CSV_Floor, Img_Floor, 0.000, 0.000, 16, 16, 1.000, 1.000, true, 282, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerWalls = addTilemap( CSV_Walls, Img_Walls, 0.000, 0.000, 16, 16, 1.000, 1.000, false, 1, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerObjects = addTilemap( CSV_Objects, Img_Objects, 0.000, 0.000, 16, 16, 1.000, 1.000, true, 1, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerRoof = addTilemap( CSV_Roof, Img_Roof, 0.000, 0.000, 16, 16, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(layerFloor);
			masterLayer.add(layerWalls);
			masterLayer.add(layerObjects);
			masterLayer.add(TrigersGroup);
			masterLayer.add(SpritesGroup);
			masterLayer.add(layerRoof);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 800;
			boundsMaxY = 800;
			boundsMin = new FlxPoint(0, 0);
			boundsMax = new FlxPoint(800, 800);
			bgColor = 0xff000000;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addShapesForLayerTrigers(onAddCallback);
			addSpritesForLayerSprites(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addShapesForLayerTrigers(onAddCallback:Function = null):void
		{
			var obj:Object;

			callbackNewData(new TextData(48.000, 73.000, 64.000, 48.000, 0.000, "Now i\rcan only\rfeel\rloneliness","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(672.000, 88.000, 64.000, 64.000, 0.000, "Why can't\r\rthey just\r\rlike me?","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(192.000, 144.000, 64.000, 32.000, 0.000, "emptiness is everywhere","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(144.000, 288.000, 64.000, 64.000, 0.000, "Why can't\rthey be\rthe way\ri want\rthem\rto be?","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(704.000, 608.000, 48.000, 50.000, 0.000, "i \rdon't\rdo this\rto them","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(128.000, 688.000, 100.000, 32.000, 0.000, "I just want them to see me the way i see myself","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			obj = new BoxData(768.000, 736.000, 0.000, 34.000, 34.000, TrigersGroup );
			shapes.push(obj);
			callbackNewData( obj, onAddCallback, TrigersGroup, generateProperties( { name:"target", value:"Level3" }, null ), 1, 1 );
		}

		public function addSpritesForLayerSprites(onAddCallback:Function = null):void
		{
			addSpriteToLayer(null, Key, SpritesGroup , 896.000, -128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"key1" }, null ), onAddCallback );//"key"
			addSpriteToLayer(null, Key, SpritesGroup , 912.000, -128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"key2" }, null ), onAddCallback );//"key"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 112.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 80.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 48.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 736.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 704.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 672.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 640.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 608.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 576.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 528.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 496.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 464.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 368.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 336.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 304.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 224.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 192.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 160.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 560.000, 32.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 32.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 0.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 63.000, 59.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 79.000, 59.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 768.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 288.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Player, SpritesGroup , 32.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"health", value:0 }, null ), onAddCallback );//"player"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 97.000, 66.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 48.000, 67.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 224.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 192.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 97.000, 85.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 46.000, 86.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 87.000, 93.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 58.000, 93.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 680.000, 94.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 712.000, 95.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Lever, SpritesGroup , 208.000, 95.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"lever1" }, null ), onAddCallback );//"lever"
			addSpriteToLayer(null, DoorH, SpritesGroup , 16.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"key", value:"key2" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 54.000, 110.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 91.000, 111.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 112.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 112.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 576.000, 112.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 544.000, 112.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 192.000, 112.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 224.000, 112.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 72.000, 113.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 712.000, 116.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 682.000, 117.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 200.000, 151.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 233.000, 152.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 80.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 48.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 112.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 336.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 624.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 768.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 736.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 592.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 240.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 176.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 144.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 352.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 464.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 448.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 496.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 96.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 80.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 368.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 512.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 177.000, 274.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 158.000, 274.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 736.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 193.000, 300.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 143.000, 300.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 752.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 560.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 384.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 528.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 167.000, 309.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 672.000, 320.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 640.000, 320.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 457.000, 320.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 150.000, 322.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 187.000, 322.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Chest, SpritesGroup , 456.000, 341.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"item", value:"key1" }, null ), onAddCallback );//"chest"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 483.000, 343.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 433.000, 343.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 168.000, 345.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 720.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 592.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 459.000, 367.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 272.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 304.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 512.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 624.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 656.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 688.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 720.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 496.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 336.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 464.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 448.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 368.000, 432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 688.000, 462.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 192.000, 464.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 176.000, 464.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 464.000, 464.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 464.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 464.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 722.000, 464.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Lever, SpritesGroup , 706.000, 478.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"lever3" }, null ), onAddCallback );//"lever"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 721.000, 495.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 96.000, 496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 224.000, 496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 128.000, 496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 640.000, 496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 624.000, 496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 624.000, 496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 687.000, 498.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 544.000, 512.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 496.000, 512.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Door, SpritesGroup , 48.000, 496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"key", value:"key1" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 528.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 528.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 144.000, 544.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 544.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 176.000, 544.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 272.000, 544.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 80.000, 544.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Lever, SpritesGroup , 418.000, 544.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"lever2" }, null ), onAddCallback );//"lever"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 720.000, 560.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 704.000, 560.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 240.000, 560.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 112.000, 560.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 560.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 560.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 336.000, 592.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 592.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 144.000, 592.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 737.000, 607.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 706.000, 607.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 304.000, 608.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 176.000, 608.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 624.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 448.000, 624.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 480.000, 624.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 717.000, 627.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 704.000, 644.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 739.000, 644.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 608.000, 656.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 576.000, 656.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 544.000, 656.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 48.000, 672.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 272.000, 688.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Gate, SpritesGroup , 288.000, 656.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever3" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 184.000, 688.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 152.000, 689.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 80.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 768.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 736.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 704.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 672.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 640.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 608.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 576.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 544.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 512.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 480.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 448.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 384.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 352.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 320.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 136.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Gate, SpritesGroup , 288.000, 672.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 202.000, 705.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 184.000, 711.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 152.000, 711.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 240.000, 720.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Gate, SpritesGroup , 288.000, 688.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever2" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 112.000, 736.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 752.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 176.000, 752.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 144.000, 752.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
