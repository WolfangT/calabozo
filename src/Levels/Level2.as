package Levels
{
    import com.newgrounds.*;

    public class Level2 extends PlayState
    {
        public function Level2():void
        {
            currentLevelClass = Level_2;
            currentClass = Level2;
        }

        override public function update():void
        {
            //normal update
            super.update();
            // check torches
            var allTorches:Boolean = true;
            for (var i:String in Torches.members)
                if (!Torches.members[i].Active)
                    allTorches = false;
            if (allTorches)
                API.unlockMedal("A Light in the Darkness");
        }
    }
}
