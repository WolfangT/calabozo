//Code generated with DAME. http://www.dambots.com

package Levels
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	// Custom imports:
import Mechanisms.*;import Enemies.*;import Items.*;
	public class Level_3 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../../assets/levels/mapCSV_3_Floor.csv", mimeType="application/octet-stream")] public var CSV_Floor:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Floor:Class;
		[Embed(source="../../assets/levels/mapCSV_3_Walls.csv", mimeType="application/octet-stream")] public var CSV_Walls:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Walls:Class;
		[Embed(source="../../assets/levels/mapCSV_3_Objects.csv", mimeType="application/octet-stream")] public var CSV_Objects:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Objects:Class;
		[Embed(source="../../assets/levels/mapCSV_3_Roof.csv", mimeType="application/octet-stream")] public var CSV_Roof:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Roof:Class;

		//Tilemaps
		public var layerFloor:FlxTilemap;
		public var layerWalls:FlxTilemap;
		public var layerObjects:FlxTilemap;
		public var layerRoof:FlxTilemap;

		//Sprites
		public var SpritesGroup:FlxGroup = new FlxGroup;

		//Shapes
		public var TrigersGroup:FlxGroup = new FlxGroup;

		//Properties


		public function Level_3(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerFloor = addTilemap( CSV_Floor, Img_Floor, 0.000, 0.000, 16, 16, 1.000, 1.000, true, 282, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerWalls = addTilemap( CSV_Walls, Img_Walls, 0.000, 0.000, 16, 16, 1.000, 1.000, false, 1, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerObjects = addTilemap( CSV_Objects, Img_Objects, 0.000, 0.000, 16, 16, 1.000, 1.000, true, 1, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerRoof = addTilemap( CSV_Roof, Img_Roof, 0.000, 0.000, 16, 16, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(layerFloor);
			masterLayer.add(layerWalls);
			masterLayer.add(layerObjects);
			masterLayer.add(TrigersGroup);
			masterLayer.add(SpritesGroup);
			masterLayer.add(layerRoof);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 1600;
			boundsMaxY = 800;
			boundsMin = new FlxPoint(0, 0);
			boundsMax = new FlxPoint(1600, 800);
			bgColor = 0xff000000;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addShapesForLayerTrigers(onAddCallback);
			addSpritesForLayerSprites(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addShapesForLayerTrigers(onAddCallback:Function = null):void
		{
			var obj:Object;

			callbackNewData(new TextData(592.000, 336.000, 64.000, 64.000, 0.000, "I need to change myself so they can't hurt me.\rAdapt.","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(1040.000, 336.000, 64.000, 64.000, 0.000, "I need to get away from what makes me suffer.\rPeople","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			obj = new BoxData(1568.000, 368.000, 0.000, 32.000, 32.000, TrigersGroup );
			shapes.push(obj);
			callbackNewData( obj, onAddCallback, TrigersGroup, generateProperties( { name:"target", value:"Win2" }, null ), 1, 1 );
			obj = new BoxData(0.000, 368.000, 0.000, 32.000, 32.000, TrigersGroup );
			shapes.push(obj);
			callbackNewData( obj, onAddCallback, TrigersGroup, generateProperties( { name:"target", value:"Win1" }, null ), 1, 1 );
		}

		public function addSpritesForLayerSprites(onAddCallback:Function = null):void
		{
			addSpriteToLayer(null, Key, SpritesGroup , 1200.000, -80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"key1" }, null ), onAddCallback );//"key"
			addSpriteToLayer(null, Key, SpritesGroup , 1200.000, -64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"key2" }, null ), onAddCallback );//"key"
			addSpriteToLayer(null, Torch, SpritesGroup , 864.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 816.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 896.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 784.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 944.000, 96.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 736.000, 96.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 976.000, 112.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 704.000, 112.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1008.000, 128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 672.000, 128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Player, SpritesGroup , 832.000, 128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"player"
			addSpriteToLayer(null, Torch, SpritesGroup , 1024.000, 144.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 656.000, 144.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 768.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 752.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 928.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 912.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1040.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 640.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1056.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 624.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1072.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 608.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 848.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 832.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1088.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 592.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 960.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 944.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 736.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 720.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1104.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 576.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1056.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1040.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 640.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 624.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 624.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1120.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 560.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 16.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 48.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 80.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 112.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 144.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 176.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 208.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 240.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 272.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 304.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 336.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 368.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 400.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 432.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 464.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 496.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 528.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1568.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1536.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1504.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1472.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1440.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1408.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1376.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1344.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1312.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1280.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1248.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1216.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1184.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1152.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 848.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 832.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 832.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 816.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 864.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 593.000, 357.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 642.000, 357.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 616.000, 357.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1088.000, 357.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1064.000, 357.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1041.000, 357.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 992.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 976.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 704.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 688.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 688.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 800.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 880.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 840.000, 376.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"torch1" }, null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 640.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 592.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, DoorHF, SpritesGroup , 1136.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"key", value:"key2" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, DoorH, SpritesGroup , 544.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"key", value:"key1" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, Torch, SpritesGroup , 800.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 880.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1041.000, 385.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1089.000, 385.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 1064.000, 386.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Torch, SpritesGroup , 617.000, 386.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"boss" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch_2"
			addSpriteToLayer(null, Boss, SpritesGroup , 832.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"boss" }, null ), onAddCallback );//"boss"
			addSpriteToLayer(null, Torch, SpritesGroup , 816.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 864.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1136.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 832.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 848.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1120.000, 432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 560.000, 432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1056.000, 448.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1040.000, 448.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 624.000, 448.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 640.000, 448.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 736.000, 480.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 720.000, 480.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 960.000, 480.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 944.000, 480.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1104.000, 480.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 576.000, 480.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 848.000, 512.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 832.000, 512.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1088.000, 512.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 592.000, 512.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1072.000, 544.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 608.000, 544.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1056.000, 560.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 624.000, 560.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 928.000, 576.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 912.000, 576.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 768.000, 576.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 752.000, 576.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1040.000, 576.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 640.000, 576.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1024.000, 592.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 656.000, 592.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 1008.000, 608.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 672.000, 608.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 976.000, 624.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 704.000, 624.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 944.000, 640.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 736.000, 640.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 896.000, 656.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 784.000, 656.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 864.000, 672.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 816.000, 672.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"activatedTorch"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
