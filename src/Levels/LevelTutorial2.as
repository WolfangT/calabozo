package Levels
{
    import com.newgrounds.*;

    public class LevelTutorial2 extends PlayState
    {
        public function LevelTutorial2():void
        {
            currentLevelClass = Level_Tutorial2;
            currentClass = LevelTutorial2;
        }

        override public function update():void
        {
            //normal update
            super.update();
        }

        override public function Continue():void
        {
            if (Foe.countLiving() == 0)
                API.unlockMedal("Cleanse Yourself");
            if (!ids['lever1'].Active)
                API.unlockMedal("Rebuild Yourself");
            //normal continue
            super.Continue();
        }
    }
}
