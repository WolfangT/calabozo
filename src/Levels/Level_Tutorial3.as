//Code generated with DAME. http://www.dambots.com

package Levels
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	// Custom imports:
import Mechanisms.*;import Enemies.*;import Items.*;
	public class Level_Tutorial3 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../../assets/levels/mapCSV_Tutorial3_Floor.csv", mimeType="application/octet-stream")] public var CSV_Floor:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Floor:Class;
		[Embed(source="../../assets/levels/mapCSV_Tutorial3_Walls.csv", mimeType="application/octet-stream")] public var CSV_Walls:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Walls:Class;
		[Embed(source="../../assets/levels/mapCSV_Tutorial3_Objects.csv", mimeType="application/octet-stream")] public var CSV_Objects:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Objects:Class;
		[Embed(source="../../assets/levels/mapCSV_Tutorial3_Roof.csv", mimeType="application/octet-stream")] public var CSV_Roof:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Roof:Class;

		//Tilemaps
		public var layerFloor:FlxTilemap;
		public var layerWalls:FlxTilemap;
		public var layerObjects:FlxTilemap;
		public var layerRoof:FlxTilemap;

		//Sprites
		public var SpritesGroup:FlxGroup = new FlxGroup;

		//Shapes
		public var TrigersGroup:FlxGroup = new FlxGroup;

		//Properties


		public function Level_Tutorial3(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerFloor = addTilemap( CSV_Floor, Img_Floor, 0.000, 0.000, 16, 16, 1.000, 1.000, true, 282, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerWalls = addTilemap( CSV_Walls, Img_Walls, 0.000, 0.000, 16, 16, 1.000, 1.000, false, 1, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerObjects = addTilemap( CSV_Objects, Img_Objects, 0.000, 0.000, 16, 16, 1.000, 1.000, true, 1, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerRoof = addTilemap( CSV_Roof, Img_Roof, 0.000, 0.000, 16, 16, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(layerFloor);
			masterLayer.add(layerWalls);
			masterLayer.add(layerObjects);
			masterLayer.add(TrigersGroup);
			masterLayer.add(SpritesGroup);
			masterLayer.add(layerRoof);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 480;
			boundsMaxY = 480;
			boundsMin = new FlxPoint(0, 0);
			boundsMax = new FlxPoint(480, 480);
			bgColor = 0xff000000;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addShapesForLayerTrigers(onAddCallback);
			addSpritesForLayerSprites(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addShapesForLayerTrigers(onAddCallback:Function = null):void
		{
			var obj:Object;

			obj = new BoxData(224.000, 0.000, 0.000, 32.000, 32.000, TrigersGroup );
			shapes.push(obj);
			callbackNewData( obj, onAddCallback, TrigersGroup, generateProperties( { name:"target", value:"Level1" }, null ), 1, 1 );
			callbackNewData(new TextData(208.000, 128.000, 64.000, 64.000, 0.000, "Many\rthings\rhide\rin\rhere","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(48.000, 208.000, 64.000, 32.000, 0.000, "But life is so full of dispair","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(384.000, 192.000, 32.000, 64.000, 0.000, "i must not loose hope","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(208.000, 256.000, 64.000, 32.000, 0.000, "Press\r'Z' to\rattack","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
		}

		public function addSpritesForLayerSprites(onAddCallback:Function = null):void
		{
			addSpriteToLayer(null, Key, SpritesGroup , 512.000, -32.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"key1" }, null ), onAddCallback );//"key"
			addSpriteToLayer(null, LifePotion, SpritesGroup , 552.000, -32.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"potion2" }, null ), onAddCallback );//"lifePotion"
			addSpriteToLayer(null, LifePotion, SpritesGroup , 536.000, -32.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"potion1" }, null ), onAddCallback );//"lifePotion"
			addSpriteToLayer(null, Key, SpritesGroup , 512.000, -16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"key2" }, null ), onAddCallback );//"key"
			addSpriteToLayer(null, DeathPotion, SpritesGroup , 552.000, -16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"potion4" }, null ), onAddCallback );//"deathPotion"
			addSpriteToLayer(null, DeathPotion, SpritesGroup , 536.000, -16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"potion3" }, null ), onAddCallback );//"deathPotion"
			addSpriteToLayer(null, Key, SpritesGroup , 512.000, 0.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"key3" }, null ), onAddCallback );//"key"
			addSpriteToLayer(null, LifePotion, SpritesGroup , 552.000, 0.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"potion6" }, null ), onAddCallback );//"lifePotion"
			addSpriteToLayer(null, LifePotion, SpritesGroup , 536.000, 0.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"potion5" }, null ), onAddCallback );//"lifePotion"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 320.000, 96.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 144.000, 96.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 208.000, 96.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 256.000, 96.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Gate, SpritesGroup , 224.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Chest, SpritesGroup , 320.000, 128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 144.000, 128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Torch, SpritesGroup , 288.000, 128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 176.000, 128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 256.000, 128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 208.000, 128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 144.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 384.000, 144.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 80.000, 144.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 64.000, 144.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 368.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 96.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 48.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 320.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 288.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 176.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 144.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 256.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 208.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 32.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Chest, SpritesGroup , 384.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"item", value:"potion1" }, null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 400.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"item", value:"potion2" }, null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 416.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"item", value:"potion6" }, null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 368.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"item", value:"potion5" }, null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 96.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"item", value:"potion4" }, null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 48.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"item", value:"potion3" }, { name:"id", value:"chest1" }, null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Torch, SpritesGroup , 320.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 288.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 256.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 208.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 176.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 144.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 72.000, 195.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 400.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, { name:"range", value:1600 }, { name:"id", value:"enemy3" }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 48.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, { name:"range", value:1600 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 379.000, 210.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 406.000, 212.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Lever, SpritesGroup , 312.000, 216.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"lever3" }, null ), onAddCallback );//"lever"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 53.000, 218.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 91.000, 219.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 32.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 64.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, { name:"range", value:1600 }, { name:"id", value:"enemy1" }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, DoorHF, SpritesGroup , 128.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"enemy3" }, { name:"key", value:"key1" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, DoorH, SpritesGroup , 336.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"lever3" }, { name:"key", value:"key3" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 381.000, 238.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 405.000, 238.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 368.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 48.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 96.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Player, SpritesGroup , 224.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"health", value:2 }, null ), onAddCallback );//"player"
			addSpriteToLayer(null, Torch, SpritesGroup , 144.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 176.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 208.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 256.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 288.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 320.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 384.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 256.000, 0.540, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 80.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 64.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 288.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 320.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 144.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 176.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 208.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 256.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 320.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 144.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 288.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 176.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 208.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 256.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, DoorF, SpritesGroup , 224.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"chest1" }, { name:"key", value:"key2" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, Torch, SpritesGroup , 256.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"torch1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 272.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 192.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 240.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, { name:"range", value:1600 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 208.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, { name:"range", value:1600 }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 272.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 192.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 224.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, { name:"range", value:1600 }, { name:"id", value:"enemy2" }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 240.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 224.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
