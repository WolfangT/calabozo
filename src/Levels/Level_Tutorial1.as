//Code generated with DAME. http://www.dambots.com

package Levels
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	// Custom imports:
import Mechanisms.*;import Enemies.*;import Items.*;
	public class Level_Tutorial1 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../../assets/levels/mapCSV_Tutorial1_Floor.csv", mimeType="application/octet-stream")] public var CSV_Floor:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Floor:Class;
		[Embed(source="../../assets/levels/mapCSV_Tutorial1_Walls.csv", mimeType="application/octet-stream")] public var CSV_Walls:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Walls:Class;
		[Embed(source="../../assets/levels/mapCSV_Tutorial1_Roof.csv", mimeType="application/octet-stream")] public var CSV_Roof:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Roof:Class;

		//Tilemaps
		public var layerFloor:FlxTilemap;
		public var layerWalls:FlxTilemap;
		public var layerRoof:FlxTilemap;

		//Sprites
		public var SpritesGroup:FlxGroup = new FlxGroup;

		//Shapes
		public var TrigersGroup:FlxGroup = new FlxGroup;

		//Properties


		public function Level_Tutorial1(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerFloor = addTilemap( CSV_Floor, Img_Floor, 0.000, 0.000, 16, 16, 1.000, 1.000, true, 282, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerWalls = addTilemap( CSV_Walls, Img_Walls, 0.000, 0.000, 16, 16, 1.000, 1.000, false, 1, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerRoof = addTilemap( CSV_Roof, Img_Roof, 0.000, 0.000, 16, 16, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(layerFloor);
			masterLayer.add(layerWalls);
			masterLayer.add(TrigersGroup);
			masterLayer.add(SpritesGroup);
			masterLayer.add(layerRoof);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 480;
			boundsMaxY = 480;
			boundsMin = new FlxPoint(0, 0);
			boundsMax = new FlxPoint(480, 480);
			bgColor = 0xff000000;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addShapesForLayerTrigers(onAddCallback);
			addSpritesForLayerSprites(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addShapesForLayerTrigers(onAddCallback:Function = null):void
		{
			var obj:Object;

			obj = new BoxData(224.000, 0.000, 0.000, 32.000, 32.000, TrigersGroup );
			shapes.push(obj);
			callbackNewData( obj, onAddCallback, TrigersGroup, generateProperties( { name:"target", value:"LevelTutorial2" }, null ), 1, 1 );
			callbackNewData(new TextData(224.000, 160.000, 64.000, 32.000, 0.000, "Follow \rthe\rLight","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(224.000, 240.000, 64.000, 32.000, 0.000, "Use the Arrow Keys to move","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(304.000, 368.000, 48.000, 64.000, 0.000, "Press X to activate the the lever","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(305.000, 288.000, 64.000, 32.000, 0.000, "I have to scape!","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(48.000, 176.000, 48.000, 48.000, 0.000, "This door needs a key","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			callbackNewData(new TextData(336.000, 80.000, 100.000, 50.000, 0.000, "Hidden torches can be lighted up to reveal secrets","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
		}

		public function addSpritesForLayerSprites(onAddCallback:Function = null):void
		{
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Gate, SpritesGroup , 224.000, 0.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"gate2" }, { name:"activator", value:"torchtriger1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 176.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 144.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 112.000, 80.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 377.000, 90.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"active", value:false }, { name:"id", value:"torchtriger1" }, null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 96.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 336.000, 96.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 351.000, 106.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 107.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 376.000, 107.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 128.000, 128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 80.000, 128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 249.000, 147.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Door, SpritesGroup , 96.000, 128.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"key", value:"key1" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, Torch, SpritesGroup , 272.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 160.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 48.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 80.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 128.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 160.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 304.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 272.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 248.000, 186.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 400.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 368.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 336.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Player, SpritesGroup , 234.000, 191.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"health", value:1 }, null ), onAddCallback );//"player"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 80.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 48.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 160.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 304.000, 208.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 368.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 336.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 248.000, 225.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 160.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 304.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 272.000, 240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 272.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 249.000, 266.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 160.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 344.000, 301.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 313.000, 302.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 192.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 320.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 320.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 224.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 304.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 352.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Gate, SpritesGroup , 320.000, 320.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"gate1" }, { name:"activator", value:"lever1" }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 368.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 306.000, 374.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 335.000, 375.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 32.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 64.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 352.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 432.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Lever, SpritesGroup , 376.000, 392.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"lever1" }, null ), onAddCallback );//"lever"
			addSpriteToLayer(null, Key, SpritesGroup , 48.000, 394.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"key1" }, null ), onAddCallback );//"key"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 352.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Torch, SpritesGroup , 304.000, 407.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 335.000, 408.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 32.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 64.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 368.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 416.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
