//Code generated with DAME. http://www.dambots.com

package Levels
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	// Custom imports:
import Mechanisms.*;import Enemies.*;import Items.*;
	public class Level_1 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../../assets/levels/mapCSV_1_Floor.csv", mimeType="application/octet-stream")] public var CSV_Floor:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Floor:Class;
		[Embed(source="../../assets/levels/mapCSV_1_Walls.csv", mimeType="application/octet-stream")] public var CSV_Walls:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Walls:Class;
		[Embed(source="../../assets/levels/mapCSV_1_Objects.csv", mimeType="application/octet-stream")] public var CSV_Objects:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Objects:Class;
		[Embed(source="../../assets/levels/mapCSV_1_Roof.csv", mimeType="application/octet-stream")] public var CSV_Roof:Class;
		[Embed(source="../../assets/srpitesheets/dungeon tileset calciumtrice.png")] public var Img_Roof:Class;

		//Tilemaps
		public var layerFloor:FlxTilemap;
		public var layerWalls:FlxTilemap;
		public var layerObjects:FlxTilemap;
		public var layerRoof:FlxTilemap;

		//Sprites
		public var SpritesGroup:FlxGroup = new FlxGroup;

		//Shapes
		public var TrigersGroup:FlxGroup = new FlxGroup;

		//Properties


		public function Level_1(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerFloor = addTilemap( CSV_Floor, Img_Floor, 0.000, 0.000, 16, 16, 1.000, 1.000, true, 282, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerWalls = addTilemap( CSV_Walls, Img_Walls, 0.000, 0.000, 16, 16, 1.000, 1.000, false, 1, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerObjects = addTilemap( CSV_Objects, Img_Objects, 0.000, 0.000, 16, 16, 1.000, 1.000, true, 1, 1, properties, onAddCallback );
			properties = generateProperties( null );
			layerRoof = addTilemap( CSV_Roof, Img_Roof, 0.000, 0.000, 16, 16, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(layerFloor);
			masterLayer.add(layerWalls);
			masterLayer.add(layerObjects);
			masterLayer.add(TrigersGroup);
			masterLayer.add(SpritesGroup);
			masterLayer.add(layerRoof);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 480;
			boundsMaxY = 480;
			boundsMin = new FlxPoint(0, 0);
			boundsMax = new FlxPoint(480, 480);
			bgColor = 0xff000000;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addShapesForLayerTrigers(onAddCallback);
			addSpritesForLayerSprites(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addShapesForLayerTrigers(onAddCallback:Function = null):void
		{
			var obj:Object;

			callbackNewData(new TextData(208.000, 87.000, 64.000, 56.120, 0.000, "When i was\rlittle i\rhated\rother people.","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
			obj = new BoxData(224.000, 448.000, 0.000, 32.000, 32.000, TrigersGroup );
			shapes.push(obj);
			callbackNewData( obj, onAddCallback, TrigersGroup, generateProperties( { name:"target", value:"Level2" }, null ), 1, 1 );
			callbackNewData(new TextData(208.000, 304.000, 64.000, 64.000, 0.000, "They were like rats,\rno more than mindless animals","system", 8, 0xffffff, "center"), onAddCallback, TrigersGroup, generateProperties( null ), 1, 1 ) ;
		}

		public function addSpritesForLayerSprites(onAddCallback:Function = null):void
		{
			addSpriteToLayer(null, LifePotion, SpritesGroup , 408.000, -96.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"potion2" }, null ), onAddCallback );//"lifePotion"
			addSpriteToLayer(null, LifePotion, SpritesGroup , 392.000, -96.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"potion1" }, null ), onAddCallback );//"lifePotion"
			addSpriteToLayer(null, Torch, SpritesGroup , 240.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 16.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 368.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 400.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 432.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 96.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 64.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 32.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Chest, SpritesGroup , 240.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"item", value:"potion2" }, null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 224.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"item", value:"potion1" }, null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 192.000, 32.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 256.000, 32.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 224.000, 74.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 240.000, 74.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 288.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 160.000, 64.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 253.000, 95.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 213.000, 96.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, DoorHF, SpritesGroup , 352.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"door"
			addSpriteToLayer(null, DoorH, SpritesGroup , 112.000, 48.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"door"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 212.000, 112.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 253.000, 112.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 288.000, 112.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 160.000, 112.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 224.000, 136.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 240.000, 136.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 256.000, 144.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Skeleton, SpritesGroup , 192.000, 144.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"showHealth", value:true }, null ), onAddCallback );//"skeleton"
			addSpriteToLayer(null, Chest, SpritesGroup , 256.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 208.000, 176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Torch, SpritesGroup , 256.000, 193.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 209.000, 194.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Chest, SpritesGroup , 320.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 288.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 256.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 224.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 192.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Chest, SpritesGroup , 160.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"chest"
			addSpriteToLayer(null, Rat, SpritesGroup , 100.000, 246.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"rat"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 320.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 288.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 224.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 192.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 160.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Rat, SpritesGroup , 96.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"rat"
			addSpriteToLayer(null, DoorHF, SpritesGroup , 128.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, Rat, SpritesGroup , 96.000, 272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"rat"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 291.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 292.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Rat, SpritesGroup , 96.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"rat"
			addSpriteToLayer(null, DoorHF, SpritesGroup , 128.000, 256.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, Rat, SpritesGroup , 96.000, 304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"rat"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 231.000, 326.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"torch1" }, null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 327.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 329.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Rat, SpritesGroup , 96.000, 320.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"rat"
			addSpriteToLayer(null, DoorHF, SpritesGroup , 128.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, DoorHF, SpritesGroup , 384.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"door"
			addSpriteToLayer(null, Rat, SpritesGroup , 96.000, 336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"id", value:"triger1" }, null ), onAddCallback );//"rat"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 208.000, 358.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, AutoTorch, SpritesGroup , 256.000, 361.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"autoTorch"
			addSpriteToLayer(null, Rat, SpritesGroup , 96.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"rat"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 240.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, DoorHF, SpritesGroup , 128.000, 320.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 240.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Rat, SpritesGroup , 96.000, 368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"rat"
			addSpriteToLayer(null, Torch, SpritesGroup , 240.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Rat, SpritesGroup , 96.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"rat"
			addSpriteToLayer(null, DoorHF, SpritesGroup , 128.000, 352.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 240.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Rat, SpritesGroup , 96.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"rat"
			addSpriteToLayer(null, Gate, SpritesGroup , 224.000, 400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"gate"
			addSpriteToLayer(null, Rat, SpritesGroup , 96.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"rat"
			addSpriteToLayer(null, DoorHF, SpritesGroup , 128.000, 384.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"torch1" }, null ), onAddCallback );//"door"
			addSpriteToLayer(null, Player, SpritesGroup , 32.000, 416.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"player"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 448.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 240.000, 448.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 448.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 240.000, 464.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Torch, SpritesGroup , 224.000, 464.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( { name:"activator", value:"triger1" }, { name:"active", value:false }, null ), onAddCallback );//"torch"
			addSpriteToLayer(null, Gate, SpritesGroup , 32.000, 432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"gate"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
