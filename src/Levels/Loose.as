package Levels
{
    import org.flixel.*;

    import Levels.*;

    public class Loose extends FlxState
    {
        public var timer:Number = 0;
        public var t1:FlxText;
        public var t2:FlxText;
        public var t3:FlxText;
        public var t4:FlxText;

        override public function create():void
        {
            FlxG.bgColor = 0xff000000;

            t1 = new FlxText(FlxG.width/2-50,10,100,"It does not matter how much i try");
            t1.alignment = "center";

            t2 = new FlxText(FlxG.width/2-50,40,100,"I cannot defeat my demons");
            t2.alignment = "center";

            t3 = new FlxText(FlxG.width/2-50,70,100,"My mind have become a");
            t3.alignment = "center";

            t4 = new FlxText(0,FlxG.height/2,FlxG.width,"Calabozo");
            t4.size = 16;
            t4.alignment = "center";
        }

        override public function update():void
        {
            timer += FlxG.elapsed;
            if (timer > 1) add(t1);
            if (timer > 3) add(t2);
            if (timer > 5) add(t3);
            if (timer > 7) add(t4);
            if (timer > 9 || FlxG.keys.ENTER)
                FlxG.switchState(new ToBeContinue());
            super.update();
        }
    }
}
