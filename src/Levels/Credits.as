package Levels
{
    import org.flixel.*;

    public class Credits extends FlxState
    {
        override public function create():void
        {
            FlxG.bgColor = 0xff000000;
            FlxG.playMusic(Assets.MusicAmbiance);

            var t:FlxText;
            t = new FlxText(0,(FlxG.height/4*0)+10,FlxG.width,"Game by Wolfang Torres and Alex Vera");
            t.alignment = "center";
            add(t);
            t = new FlxText(0,(FlxG.height/4*1)+10,FlxG.width,"Art by Calciumtrice");
            t.alignment = "center";
            add(t);
            t = new FlxText(0,(FlxG.height/4*1)+20,FlxG.width,"calciumtrice.tumblr.com");
            t.alignment = "center";
            t.size = 8;
            add(t);
            t = new FlxText(0,(FlxG.height/4*2)+10,FlxG.width,"Music by Jesús Lastra");
            t.alignment = "center";
            add(t);
            t = new FlxText(0,(FlxG.height/4*2)+20,FlxG.width,"youtube.com/channel/UCdmZ_18CDUgpRymmLBqLLUQ");
            t.alignment = "center";
            t.size = 8;
            add(t);
            t = new FlxText(0,(FlxG.height/4*3)+10,FlxG.width,"Sounds by Jute");
            t.alignment = "center";
            add(t);
            t = new FlxText(0,(FlxG.height/4*3)+40,FlxG.width,"Press SPACE to return");
            t.alignment = "center";
            t.size = 6;
            add(t);
        }

        override public function update():void
        {
            if (FlxG.keys.SPACE)
            {
                FlxG.music.stop();
                FlxG.switchState(new Menu());
            }
            super.update();
        }
    }
}
