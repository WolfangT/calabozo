package Levels
{
    import org.flixel.*;

    public class Menu extends FlxState
    {
        override public function create():void
        {
            FlxG.bgColor = 0xff000000;

            var t:FlxText;
            t = new FlxText(0,FlxG.height/2-50,FlxG.width,"Calabozo");
            t.size = 16;
            t.alignment = "center";
            add(t);
            t = new FlxText(FlxG.width/2-50,FlxG.height-50,100,"Press ENTER to start o SPACE to see the credits");
            t.alignment = "center";
            add(t);
        }

        override public function update():void
        {
            if (FlxG.keys.ENTER)
                FlxG.switchState(new Intro());
            if (FlxG.keys.SPACE)
                FlxG.switchState(new Credits());
            super.update();
        }
    }
}
