﻿package Levels
{
    import org.flixel.FlxSprite;

    public class Trigger extends FlxSprite
    {
        public var target:String = "";
        public var targetObject:Object = null;

        public function Trigger( X:Number, Y:Number, Width:uint, Height:uint )
        {
            super(X, Y);
            width = Width;
            height = Height;
            visible = false;
        }

    }

}
