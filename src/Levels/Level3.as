package Levels
{
    import org.flixel.*;

    public class Level3 extends PlayState
    {
        public function Level3():void
        {
            currentLevelClass = Level_3;
            currentClass = Level3;
        }

        override public function update():void
        {
            //normal update
            super.update();
            // activate boss
            if (ids["torch1"].Active)
                ids["boss"].Thinking = true;
            if (!ids["boss"].alive)
                ids["torch1"].Active = false;
        }

        override public function End():void
        {
            FlxG.switchState(new Loose());
        }
    }
}
