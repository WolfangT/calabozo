package Items
{
    import org.flixel.*;

    public class Item extends FlxSprite
    {
        public var Image:Class;
        public var name:String;

        public function Item(X:Number,Y:Number):void
        {
            super(X,Y)
            loadGraphic(Image,true,true,16,16,true);
        }

        public function Use(character:Character):void
        {
        }
    }
}
