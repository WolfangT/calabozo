package Items
{
    public class AtackSpeedPotion extends Potion
    {
        public function AtackSpeedPotion(X:Number,Y:Number):void
        {
            Image = Assets.ImagePotionGreen;
            name = "Essence of Hate";
            super(X,Y);
        }

        override public function Use(character:Character):void
        {
            character.atackDelay *= 0.8;
        }
    }
}
