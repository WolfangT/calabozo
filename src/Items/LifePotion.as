package Items
{
    public class LifePotion extends Potion
    {
        public function LifePotion(X:Number,Y:Number):void
        {
            Image = Assets.ImagePotionRed;
            name = "Essence of Hope";
            super(X,Y);
        }

        override public function Use(character:Character):void
        {
            character.health++;
        }
    }
}
