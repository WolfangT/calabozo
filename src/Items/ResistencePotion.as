package Items
{
    public class ResistencePotion extends Potion
    {
        public function ResistencePotion(X:Number,Y:Number):void
        {
            Image = Assets.ImagePotionPurple;
            name = "Essence of Will";
            super(X,Y);
        }

        override public function Use(character:Character):void
        {
            character.hurtDelay *= 0.8;
        }
    }
}
