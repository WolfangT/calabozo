package Items
{
    public class Key extends Item
    {
        public function Key(X:Number,Y:Number):void
        {
            Image = Assets.ImageKey;
            name = "Key";
            super(X,Y);
        }
    }
}
