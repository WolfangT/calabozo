package Items
{
    public class DeathPotion extends Potion
    {
        public function DeathPotion(X:Number,Y:Number):void
        {
            Image = Assets.ImagePotionBlack;
            name = "Essence of Despair";
            super(X,Y);
        }

        override public function Use(character:Character):void
        {
            character.health--;
        }
    }
}
