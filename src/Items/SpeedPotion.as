package Items
{
    public class SpeedPotion extends Potion
    {
        public function SpeedPotion(X:Number,Y:Number):void
        {
            Image = Assets.ImagePotionBlue;
            name = "Essence of Desire";
            super(X,Y);
        }

        override public function Use(character:Character):void
        {
            character.maxVelocity.x += 25;
            character.maxVelocity.y += 25;
        }
    }
}
