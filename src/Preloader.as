package
{
    import com.newgrounds.*;
    import com.newgrounds.components.*;
    import flash.display.MovieClip;
    import flash.display.Shape;
    import flash.display.Graphics;

    public class Preloader extends MovieClip
    {
        public function Preloader()
        {
            // background
            var background:Shape = new Shape();
            background.graphics.beginFill(0x000000,1);
            background.graphics.drawRect(0,0,640,480);
            addChild(background);
            // API
            var apiConnector:APIConnector = new APIConnector();
            apiConnector.className = "Calabozo";
            apiConnector.apiId = "41909:4TW4MqTd";
            apiConnector.encryptionKey = "9Mbm9TDiC9d5RoW6R5kocC0mmSEnCiX5";
            addChild(apiConnector);
            // center connector on screen
            if(stage)
            {
                apiConnector.x = (stage.stageWidth - apiConnector.width) / 2;
                apiConnector.y = (stage.stageHeight - apiConnector.height) / 2;
            }
        }
    }
}
