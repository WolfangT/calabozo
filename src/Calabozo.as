package
{
    import flash.events.Event;
    import com.newgrounds.components.*;
    import org.flixel.*;
    import Levels.*;
    [SWF(width="640", height="480", frameRate="30")]
    [Frame(factoryClass="Preloader")]

    public class Calabozo extends FlxGame
    {
        public function Calabozo()
        {
            super(320, 240, Menu, 2);
        }

        override protected function create(FlashEvent:Event):void
        {
            super.create(FlashEvent);
            var medalPopup:MedalPopup = new MedalPopup();
            medalPopup.x = medalPopup.y = 5;
            addChild(medalPopup);
        }
    }
}
