 package
{
    import org.flixel.*;
    import Items.*;

    public class Character extends FlxSprite
    {
        public var attacking:Boolean = false;
        public var interacting:Boolean = false;
        public var waking:Boolean = false;
        public var hurted:Boolean = false;

        public var hurtDelay:Number = 0.5;
        public var hurtTimer:Number = 0;

        public var atackDelay:Number = 1;
        public var preAtackTimer:Number = 0;
        public var postAtackTimer:Number = 0;

        public var Inventory:FlxGroup = new FlxGroup(1);

        public var showHealth:Boolean = false;
        public var healthIndicator:Health;
        public var showLight:Boolean = false;
        public var light:Light;

        public var SoundAttack:Class = Assets.SoundAttack;

        public function Character(X:Number, Y:Number):void
        {
            super(X,Y);
            // crear graficas y animaciones
            addAnimation("stand", [0,1,2,3,4,5,6,7,8,9], 3, true);
            addAnimation("wait", [10,11,12,13,14,15,16,17,18,19], 10, false);
            addAnimation("walk", [20,21,22,23,24,25,26,27,28,29], 20, true);
            addAnimation("attack", [30,31,32,33,34,35,36,37,38,39], 10, false);
            addAnimation("die", [40,41,42,43,44,45,46,47,48,49], 20, false);
            addAnimation("wake", [49,48,47,46,45,44,43,40,41,40], 15, false);
            facing = RIGHT;
            // fisica
            maxVelocity.x = 100;
            maxVelocity.y = 100;
            drag.x = maxVelocity.x*5;
            drag.y = maxVelocity.y*5;
            // game stuff
            health = 3;
            healthIndicator = new Health(this);
            light = new Light(this, "n");
        }

        override public function update():void
        {
            // Run estandar update
            super.update();
            healthIndicator.update();
            light.update();
            // movement
            acceleration.x = 0;
            acceleration.y = 0;
            // check if alive
            if (!alive)
                return;
            // change facing
            if (velocity.x > 0)
                facing = RIGHT;
            if (velocity.x < 0)
                facing = LEFT;
            // light
            if (health > 3) light.size = "e";
            if (health == 3) light.size = "g";
            if (health == 2) light.size = "n";
            if (health == 1) light.size = "p";
            // wake animation
            if (waking)
            {
                if (finished)
                    waking = false;
            }
            // being hurt
            if (hurted)
            {
                hurtTimer += FlxG.elapsed;
                if (hurtTimer > hurtDelay)
                {
                    hurted = false;
                    hurtTimer = 0;
                }
            }
            // interact with something
            if (interacting)
            {
                preAtackTimer += FlxG.elapsed;
                if (preAtackTimer > (atackDelay/2))
                {
                    if (postAtackTimer == 0)
                    {
                        var action:Projectile;
                        action = (FlxG.state as PlayState).Actions.recycle() as Projectile;
                        action.reset(x-1, y-1);
                        action.source = this;
                    }
                    postAtackTimer += FlxG.elapsed;
                    if (postAtackTimer > (atackDelay/2))
                    {
                        interacting = false;
                        preAtackTimer = 0;
                        postAtackTimer = 0;
                    }
                }
            }
            // make an attack
            if (attacking)
            {
                preAtackTimer += FlxG.elapsed;
                if (preAtackTimer > (atackDelay/2))
                {
                    if (postAtackTimer == 0)
                    {
                        var projectile:Projectile;
                        projectile = (FlxG.state as PlayState).Projectiles.recycle() as Projectile;
                        if (facing == RIGHT)
                        {
                            projectile.reset(x+width,y-1);
                            x += 1;
                        }
                        else
                        {
                            projectile.reset(x-projectile.width,y-1);
                            x -= 1;
                        }
                        projectile.source = this;
                    }
                    postAtackTimer += FlxG.elapsed;
                    if (postAtackTimer > (atackDelay/2))
                    {
                        attacking = false;
                        preAtackTimer = 0;
                        postAtackTimer = 0;
                    }
                }
            }
            // Change animation
            if (waking)
                play('wake');
            else if (attacking)
                play("attack");
            else if (interacting)
                play('wait');
            else
            {
                if (velocity.y != 0 || velocity.x != 0)
                    play("walk");
                else
                    play("stand");
            }
        }

        override public function draw():void
        {
            super.draw();
            if (showHealth && health > 0)
                healthIndicator.draw();
            if (showLight && health > 0)
                light.draw();
        }

        public function Interact():void
        {
            interacting = true;
            preAtackTimer = 0;
            postAtackTimer = 0;
        }

        public function Attack():void
        {
            attacking = true;
            preAtackTimer = 0;
            postAtackTimer = 0;
            FlxG.play(SoundAttack, 0.2);
        }

        public function Take(item:Item):void
        {
            if (item is Potion)
                Use(item);
            else
            {
                if (Inventory.members.length > 0)
                    Drop();
                Inventory.add(item);
                item.visible = false;
            }
        }

        public function Drop():void
        {
            if (Inventory.members.length > 0)
            {
                var item:Item;
                item = (Inventory.remove(Inventory.members[0], true) as Item);
                (FlxG.state as PlayState).sprites.add(item);
                item.visible = true;
                item.x = x;
                item.y = y+16;
            }
        }

        public function Use(item:Item=null):void
        {
            if (item == null)
                var item:Item = (Inventory.members[0] as Item);
            item.Use(this);
            (FlxG.state as PlayState).status.text = item.name;
        }

        override public function hurt(Damage:Number):void
        {
            if (!hurted)
            {
                super.hurt(Damage);
                flicker(hurtDelay);
                hurtTimer = 0;
                hurted = true;
                // extra effect
                attacking = false;
            }
        }

        override public function kill():void
        {
            alive = false;
            play("die");
        }
    }
}


import org.flixel.*;

class Health extends FlxText
{
    public var cha:Character;

    public function Health(Cha:Character):void
    {
        cha = Cha;
        super(cha.x-26, cha.y-32, 64);
        shadow = 0xff000000;
        alignment = "center";
    }

    override public function update():void
    {
        text = "SALUD: "+(cha.health);
        x = cha.x - 26;
        y = cha.y - 32;
    }
}
