package
{
    public class Assets
    {
        [Embed(source="../assets/srpitesheets/warrior spritesheet calciumtrice.png")]
        public static var ImageWarrior:Class;

        [Embed(source="../assets/srpitesheets/skeleton spritesheet calciumtrice.png")]
        public static var ImageSkeleton:Class;

        [Embed(source="../assets/srpitesheets/rat spritesheet calciumtrice.png")]
        public static var ImageRat:Class;

        [Embed(source="../assets/srpitesheets/red-potion.png")]
        public static var ImagePotionRed:Class;

        [Embed(source="../assets/srpitesheets/purple-potion.png")]
        public static var ImagePotionPurple:Class;

        [Embed(source="../assets/srpitesheets/blue-potion.png")]
        public static var ImagePotionBlue:Class;

        [Embed(source="../assets/srpitesheets/green-potion.png")]
        public static var ImagePotionGreen:Class;

        [Embed(source="../assets/srpitesheets/yellow-potion.png")]
        public static var ImagePotionYellow:Class;

        [Embed(source="../assets/srpitesheets/pink-potion.png")]
        public static var ImagePotionPink:Class;

        [Embed(source="../assets/srpitesheets/black-potion.png")]
        public static var ImagePotionBlack:Class;

        [Embed(source="../assets/srpitesheets/door.png")]
        public static var ImageDoor:Class;

        [Embed(source="../assets/srpitesheets/door_f.png")]
        public static var ImageDoorF:Class;

        [Embed(source="../assets/srpitesheets/door_h.png")]
        public static var ImageDoorH:Class;

        [Embed(source="../assets/srpitesheets/door_hf.png")]
        public static var ImageDoorHF:Class;

        [Embed(source="../assets/srpitesheets/gate_hf.png")]
        public static var ImageGateHF:Class;

        [Embed(source="../assets/srpitesheets/gate.png")]
        public static var ImageGate:Class;

        [Embed(source="../assets/srpitesheets/chest.png")]
        public static var ImageChest:Class;

        [Embed(source="../assets/srpitesheets/lever.png")]
        public static var ImageLever:Class;

        [Embed(source="../assets/srpitesheets/torch.png")]
        public static var ImageTorch:Class;

        [Embed(source="../assets/srpitesheets/key.png")]
        public static var ImageKey:Class;

        [Embed(source="../assets/srpitesheets/glow-light_64.png")]
        public static var light_p:Class;

        [Embed(source="../assets/srpitesheets/glow-light_128.png")]
        public static var light_n:Class;

        [Embed(source="../assets/srpitesheets/glow-light_256.png")]
        public static var light_g:Class;

        [Embed(source="../assets/srpitesheets/glow-light_512.png")]
        public static var light_e:Class;

        [Embed(source="../assets/music/Jesús Lastra - Abandoned.mp3")]
        public static var MusicAmbiance:Class;

        [Embed(source="../assets/sounds/doorwood_open.mp3")]
        public static var SoundActivateDoor:Class;

        [Embed(source="../assets/sounds/doorwood_close.mp3")]
        public static var SoundDeactivateDoor:Class;

        [Embed(source="../assets/sounds/click_1.mp3")]
        public static var SoundActivate:Class;

        [Embed(source="../assets/sounds/click_2.mp3")]
        public static var SoundDeactivate:Class;

        [Embed(source="../assets/sounds/hit_1.mp3")]
        public static var SoundAttack:Class;

        [Embed(source="../assets/sounds/skull_hit_1.mp3")]
        public static var SoundAttackSkeleton:Class;
    }
}
