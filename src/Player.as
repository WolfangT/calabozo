package
{
    import org.flixel.*;
    import Levels.*;

    public class Player extends Character
    {
        public function Player(X:Number, Y:Number):void
        {
            super(X,Y);
            loadGraphic(Assets.ImageWarrior, true, true, 32, 32, true);
            // modificar hitbox
            width = 14;
            height = 14;
            offset.x = 9;
            offset.y = 18;
            // game stuff
            health = 3;
            waking = true;
            showLight = true;
        }

        override public function update():void
        {
            // Run estandar update
            super.update();
            //recive keys
            if (waking || interacting || attacking || hurted)
            {
                velocity.x = 0;
                velocity.y = 0;
            }
            else
            {
                if (FlxG.keys.UP) acceleration.y = -maxVelocity.y*4;
                if (FlxG.keys.DOWN) acceleration.y = maxVelocity.y*4;
                if (FlxG.keys.RIGHT) acceleration.x = maxVelocity.x*4;
                if (FlxG.keys.LEFT) acceleration.x = -maxVelocity.x*4;
                if (FlxG.keys.justPressed("Z")) Attack();
                if (FlxG.keys.justPressed("X")) Interact();
                if (FlxG.keys.justPressed("C")) Drop();
            }
        }

        override public function hurt(Damage:Number):void
        {
            super.hurt(Damage);
            FlxG.camera.shake(0.01);
        }
    }
}
