package
{
    import org.flixel.*;

    public class Light extends FlxSprite
    {
        public var darkness:FlxSprite;
        public var source:FlxSprite;
        public var size:String;
        public var image:Class = Assets.light_n;

        public function Light(Source:FlxSprite, Size:String):void
        {
            super(0,0);
            source = Source;
            darkness = (FlxG.state as PlayState).darkness;
            size = Size;
            blend = "screen";
        }

        override public function update():void
        {
            // size
            if (size=="p") image = Assets.light_p;
            if (size=="n") image = Assets.light_n;
            if (size=="g") image = Assets.light_g;
            if (size=="e") image = Assets.light_e;
            loadGraphic(image);
            // position
            x = source.x + 8;
            y = source.y;
            super.update();
        }

        override public function draw():void
        {
            var screenXY:FlxPoint = getScreenXY();
            if (darkness != null)
                darkness.stamp(this,
                     screenXY.x - (this.width/2),
                     screenXY.y - (this.height/2));
        }
    }
}
